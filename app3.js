//file system
var fs=require('fs')
    fs.readFile('./demo.html','utf8',(err,data)=>{
        if(err) {
            console.log(err);
        }
        console.log(data);
    })

    
fs.writeFile('file.text','welcome Node js',(err)=>{
    if(err) {
        console.log(err);
    }
    console.log('replaced')
})
    
fs.appendFile('file.text','Hello content',(err)=>{
    if(err){
        console.log(err);
    }
    console.log('data added');
})


fs.rename('day1.html','day2.html',(err)=>{
    if(err) {
        console.log(err)
    }
    console.log('file renamed')
})

fs.unlink('file1.text',(err)=>{
    if(err) {
        console.log(err);
    }
    console.log('deleted')
})